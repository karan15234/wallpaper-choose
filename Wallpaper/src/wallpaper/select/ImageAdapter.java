package wallpaper.select;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;


/*
 * This class is the adapter class for the grid view
 * Grid View needs an adapter class to add objects into
 * the grid
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;
 
    /*
     * all the ids of the images go here, the images should be present
     * in the folder 'drawable' in /res/layout 
     */
    public Drawable[] images;
    
    public ImageAdapter(Context c){
        mContext = c;
        images = new Drawable[16];
        for (int j = 1; j <= 16; j++) {
            images[j-1]=mContext.getResources().getDrawable(mContext.getResources()
                  .getIdentifier("bg"+j, "drawable", mContext.getPackageName()));
        }
    }
 
    @Override
    public int getCount() {
        return images.length;
    }
 
    @Override
    public Object getItem(int position) {
        return images[position];
    }
    
    /*
     * returns the index of selected image in the array 'images'
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    /*
     * adds the images into the grid view
     * and manage image size in the grid
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	ImageView imageView = new ImageView(mContext);
    	imageView.setImageDrawable(images[position]);
    	imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    	WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	imageView.setLayoutParams(new GridView.LayoutParams(display.getWidth()/4, display.getHeight()/4));
        return imageView;
    }
}
