package wallpaper.select;

import java.util.HashMap;

import com.flurry.android.FlurryAgent;

import android.app.Activity;
import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SwipeGesture extends SimpleOnGestureListener implements OnTouchListener{
        
	Context context;
	GestureDetector gDetector;
	int position=-1;	
	/*
	 * contains the position of the image being viewed in the full view
	 */
	Activity activity;
	float prev=0;
	
	public SwipeGesture()
	{
		super();
	}

	/*
	 * Constructor for this class with parameters Activity and int
	 */
	public SwipeGesture(Activity activity,int position) {
		this(activity.getApplicationContext(), null);
		this.position=position;
		this.activity=activity;
	}

	/*
	 * Constructor for this class with parameters Context and GestureDetector
	 */
	public SwipeGesture(Context context, GestureDetector gDetector) {

		if(gDetector == null)
			gDetector = new GestureDetector(context, this);

		this.context = context;
		this.gDetector = gDetector;
	}

	public boolean onTouch(View v, MotionEvent event) {
		synchronized (event)
		{
			/*
			 * check the position when finger is released
			 */
			if(event.getAction()==MotionEvent.ACTION_UP)
			{
				float diff=prev-event.getX();
				ImageAdapter imageAdapter = new ImageAdapter(activity);
				if(diff<-60)
				{
					position--;
					if(position==-1)
						position=imageAdapter.getCount()-1;
				}
				else if(diff>60)
				{
					position++;
					if(position==imageAdapter.getCount())
						position=0;
				}
				HashMap<String,String> map = new HashMap<String,String>();
            	map.put(Integer.toString(position), "bg"+position);
				FlurryAgent.logEvent("Chosen from Swipe", map);
				WallpaperActivity.FullView(activity,position);
			}
			
			/*
			 *check the position where finger is pressed
			 *and record that X coordinate into prev 
			 */
			if(event.getAction()==MotionEvent.ACTION_DOWN)
			{
				prev=event.getX();
			}
		}
		return true;
	}


	public GestureDetector getDetector()
	{
		return gDetector;
	}       
}
