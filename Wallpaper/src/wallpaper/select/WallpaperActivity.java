package wallpaper.select;

import java.io.IOException;
import java.util.HashMap;
import android.app.Activity;
import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

/*
 * This is the main class which is called when the app starts
 * Manages all the 3 screens
 */
public class WallpaperActivity extends Activity {
    /** Called when the activity is first created. */
	
	boolean showImage=false;
	
	/* 
	 * checks if full image view was set
	 * returns back to earlier grid layout
	 * otherwise exits the application 
	 */
	@Override
	public void onBackPressed() {
			if(showImage)
			{
				Image_Grid();
			}
			else
				finish();
	}
	
	public void onStart()
	{
	   super.onStart();
	   FlurryAgent.onStartSession(this, "64QDMNH7S7CMJXWTKR66");
	   LinearLayout splash=(LinearLayout)findViewById(R.id.splash_image);
       splash.setBackgroundResource(R.drawable.splash);
       Thread splashThread = new Thread() {
       	Handler handler=new Handler();
       @Override
       public void run() {
       	handler.postDelayed(new Runnable(){
				@Override
				public void run() {
					Image_Grid();
				}
       	}, 2000);
       }
       };
       splashThread.start();
	}
	
	public void onStop()
	{
	   super.onStop();
	   FlurryAgent.onEndSession(this);
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
    }
    
    /*
     * sets the view to main.xml - xml showing image grid
     * calls class ImageAdapter for the grid view
     * implements and manages click event on the image
     */
    public void Image_Grid()	
    {
    	showImage=false;
    	setContentView(R.layout.main);
        GridView gridView = (GridView) findViewById(R.id.grid_view);
        gridView.setAdapter(new ImageAdapter(this));
        
        /*
         * sense click event and show full image on click over any image
         * layout set to full.xml which shows the full view of the image 
         */
        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                    int position, long id) {
            	showImage=true;
            	FullView(WallpaperActivity.this,position);
            	HashMap<String,String> map = new HashMap<String,String>();
            	map.put(Integer.toString(position), "bg"+position);
            	FlurryAgent.logEvent("Chosen from Grid", map);
            }
        });
    }
    
    public static void FullView(final Activity activity,int position)
    {
    	activity.setContentView(R.layout.full);
    	RelativeLayout layout=(RelativeLayout)activity.findViewById(R.id.layout);
    	final ImageAdapter imageAdapter = new ImageAdapter(activity);
    	layout.setBackgroundDrawable(imageAdapter.images[position]);
    	
    	Button b=(Button)activity.findViewById(R.id.set);
    	final int pos=position;
    	
    	SwipeGesture swipe = new SwipeGesture(activity,position);
        activity.findViewById(R.id.layout).setOnTouchListener(swipe);
    	
    	/*
    	 * senses if button 'Set  As Wallpaper' is clicked 
    	 * and sets the image as wallpaper
    	 */
    	b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	HashMap<String,String> map = new HashMap<String,String>();
            	map.put(Integer.toString(pos), "bg"+pos);
				FlurryAgent.logEvent("Wallpaper Set", map);
				WallpaperActivity.FullView(activity,pos);
            	WallpaperManager wpm;
                wpm=WallpaperManager.getInstance(activity);
                Drawable d=imageAdapter.images[pos];
                
                Bitmap wallpaper=((BitmapDrawable)d).getBitmap();
                try {
                	/*
                	 * requires permission - android.permission.SET_WALLPAPER 
                	 */
                    wpm.setBitmap(wallpaper);
                    activity.finish();
                } catch (IOException e) {
                }
            }
    	});
    }
}